//
// This is a generated file, do not edit!
// Generated by R.swift, see https://github.com/mac-cain13/R.swift
//

import Foundation
import Rswift
import UIKit

/// This `R` struct is generated and contains references to static resources.
struct R: Rswift.Validatable {
  fileprivate static let applicationLocale = hostingBundle.preferredLocalizations.first.flatMap(Locale.init) ?? Locale.current
  fileprivate static let hostingBundle = Bundle(for: R.Class.self)

  /// Find first language and bundle for which the table exists
  fileprivate static func localeBundle(tableName: String, preferredLanguages: [String]) -> (Foundation.Locale, Foundation.Bundle)? {
    // Filter preferredLanguages to localizations, use first locale
    var languages = preferredLanguages
      .map(Locale.init)
      .prefix(1)
      .flatMap { locale -> [String] in
        if hostingBundle.localizations.contains(locale.identifier) {
          if let language = locale.languageCode, hostingBundle.localizations.contains(language) {
            return [locale.identifier, language]
          } else {
            return [locale.identifier]
          }
        } else if let language = locale.languageCode, hostingBundle.localizations.contains(language) {
          return [language]
        } else {
          return []
        }
      }

    // If there's no languages, use development language as backstop
    if languages.isEmpty {
      if let developmentLocalization = hostingBundle.developmentLocalization {
        languages = [developmentLocalization]
      }
    } else {
      // Insert Base as second item (between locale identifier and languageCode)
      languages.insert("Base", at: 1)

      // Add development language as backstop
      if let developmentLocalization = hostingBundle.developmentLocalization {
        languages.append(developmentLocalization)
      }
    }

    // Find first language for which table exists
    // Note: key might not exist in chosen language (in that case, key will be shown)
    for language in languages {
      if let lproj = hostingBundle.url(forResource: language, withExtension: "lproj"),
         let lbundle = Bundle(url: lproj)
      {
        let strings = lbundle.url(forResource: tableName, withExtension: "strings")
        let stringsdict = lbundle.url(forResource: tableName, withExtension: "stringsdict")

        if strings != nil || stringsdict != nil {
          return (Locale(identifier: language), lbundle)
        }
      }
    }

    // If table is available in main bundle, don't look for localized resources
    let strings = hostingBundle.url(forResource: tableName, withExtension: "strings", subdirectory: nil, localization: nil)
    let stringsdict = hostingBundle.url(forResource: tableName, withExtension: "stringsdict", subdirectory: nil, localization: nil)

    if strings != nil || stringsdict != nil {
      return (applicationLocale, hostingBundle)
    }

    // If table is not found for requested languages, key will be shown
    return nil
  }

  /// Load string from Info.plist file
  fileprivate static func infoPlistString(path: [String], key: String) -> String? {
    var dict = hostingBundle.infoDictionary
    for step in path {
      guard let obj = dict?[step] as? [String: Any] else { return nil }
      dict = obj
    }
    return dict?[key] as? String
  }

  static func validate() throws {
    try font.validate()
    try intern.validate()
  }

  #if os(iOS) || os(tvOS)
  /// This `R.storyboard` struct is generated, and contains static references to 6 storyboards.
  struct storyboard {
    /// Storyboard `LaunchScreen`.
    static let launchScreen = _R.storyboard.launchScreen()
    /// Storyboard `MovieDetails_iPad`.
    static let movieDetails_iPad = _R.storyboard.movieDetails_iPad()
    /// Storyboard `MovieDetails`.
    static let movieDetails = _R.storyboard.movieDetails()
    /// Storyboard `MoviesList_iPad`.
    static let moviesList_iPad = _R.storyboard.moviesList_iPad()
    /// Storyboard `MoviesList`.
    static let moviesList = _R.storyboard.moviesList()
    /// Storyboard `TabBar`.
    static let tabBar = _R.storyboard.tabBar()

    #if os(iOS) || os(tvOS)
    /// `UIStoryboard(name: "LaunchScreen", bundle: ...)`
    static func launchScreen(_: Void = ()) -> UIKit.UIStoryboard {
      return UIKit.UIStoryboard(resource: R.storyboard.launchScreen)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIStoryboard(name: "MovieDetails", bundle: ...)`
    static func movieDetails(_: Void = ()) -> UIKit.UIStoryboard {
      return UIKit.UIStoryboard(resource: R.storyboard.movieDetails)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIStoryboard(name: "MovieDetails_iPad", bundle: ...)`
    static func movieDetails_iPad(_: Void = ()) -> UIKit.UIStoryboard {
      return UIKit.UIStoryboard(resource: R.storyboard.movieDetails_iPad)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIStoryboard(name: "MoviesList", bundle: ...)`
    static func moviesList(_: Void = ()) -> UIKit.UIStoryboard {
      return UIKit.UIStoryboard(resource: R.storyboard.moviesList)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIStoryboard(name: "MoviesList_iPad", bundle: ...)`
    static func moviesList_iPad(_: Void = ()) -> UIKit.UIStoryboard {
      return UIKit.UIStoryboard(resource: R.storyboard.moviesList_iPad)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIStoryboard(name: "TabBar", bundle: ...)`
    static func tabBar(_: Void = ()) -> UIKit.UIStoryboard {
      return UIKit.UIStoryboard(resource: R.storyboard.tabBar)
    }
    #endif

    fileprivate init() {}
  }
  #endif

  /// This `R.file` struct is generated, and contains static references to 3 files.
  struct file {
    /// Resource file `Roboto-Medium.ttf`.
    static let robotoMediumTtf = Rswift.FileResource(bundle: R.hostingBundle, name: "Roboto-Medium", pathExtension: "ttf")
    /// Resource file `SFProText-Regular.ttf`.
    static let sfProTextRegularTtf = Rswift.FileResource(bundle: R.hostingBundle, name: "SFProText-Regular", pathExtension: "ttf")
    /// Resource file `SFProText-Semibold.ttf`.
    static let sfProTextSemiboldTtf = Rswift.FileResource(bundle: R.hostingBundle, name: "SFProText-Semibold", pathExtension: "ttf")

    /// `bundle.url(forResource: "Roboto-Medium", withExtension: "ttf")`
    static func robotoMediumTtf(_: Void = ()) -> Foundation.URL? {
      let fileResource = R.file.robotoMediumTtf
      return fileResource.bundle.url(forResource: fileResource)
    }

    /// `bundle.url(forResource: "SFProText-Regular", withExtension: "ttf")`
    static func sfProTextRegularTtf(_: Void = ()) -> Foundation.URL? {
      let fileResource = R.file.sfProTextRegularTtf
      return fileResource.bundle.url(forResource: fileResource)
    }

    /// `bundle.url(forResource: "SFProText-Semibold", withExtension: "ttf")`
    static func sfProTextSemiboldTtf(_: Void = ()) -> Foundation.URL? {
      let fileResource = R.file.sfProTextSemiboldTtf
      return fileResource.bundle.url(forResource: fileResource)
    }

    fileprivate init() {}
  }

  /// This `R.font` struct is generated, and contains static references to 3 fonts.
  struct font: Rswift.Validatable {
    /// Font `Roboto-Medium`.
    static let robotoMedium = Rswift.FontResource(fontName: "Roboto-Medium")
    /// Font `SFProText-Regular`.
    static let sfProTextRegular = Rswift.FontResource(fontName: "SFProText-Regular")
    /// Font `SFProText-Semibold`.
    static let sfProTextSemibold = Rswift.FontResource(fontName: "SFProText-Semibold")

    /// `UIFont(name: "Roboto-Medium", size: ...)`
    static func robotoMedium(size: CGFloat) -> UIKit.UIFont? {
      return UIKit.UIFont(resource: robotoMedium, size: size)
    }

    /// `UIFont(name: "SFProText-Regular", size: ...)`
    static func sfProTextRegular(size: CGFloat) -> UIKit.UIFont? {
      return UIKit.UIFont(resource: sfProTextRegular, size: size)
    }

    /// `UIFont(name: "SFProText-Semibold", size: ...)`
    static func sfProTextSemibold(size: CGFloat) -> UIKit.UIFont? {
      return UIKit.UIFont(resource: sfProTextSemibold, size: size)
    }

    static func validate() throws {
      if R.font.robotoMedium(size: 42) == nil { throw Rswift.ValidationError(description:"[R.swift] Font 'Roboto-Medium' could not be loaded, is 'Roboto-Medium.ttf' added to the UIAppFonts array in this targets Info.plist?") }
      if R.font.sfProTextRegular(size: 42) == nil { throw Rswift.ValidationError(description:"[R.swift] Font 'SFProText-Regular' could not be loaded, is 'SFProText-Regular.ttf' added to the UIAppFonts array in this targets Info.plist?") }
      if R.font.sfProTextSemibold(size: 42) == nil { throw Rswift.ValidationError(description:"[R.swift] Font 'SFProText-Semibold' could not be loaded, is 'SFProText-Semibold.ttf' added to the UIAppFonts array in this targets Info.plist?") }
    }

    fileprivate init() {}
  }

  /// This `R.image` struct is generated, and contains static references to 9 images.
  struct image {
    /// Image `Close`.
    static let close = Rswift.ImageResource(bundle: R.hostingBundle, name: "Close")
    /// Image `cap`.
    static let cap = Rswift.ImageResource(bundle: R.hostingBundle, name: "cap")
    /// Image `no_image`.
    static let no_image = Rswift.ImageResource(bundle: R.hostingBundle, name: "no_image")
    /// Image `play`.
    static let play = Rswift.ImageResource(bundle: R.hostingBundle, name: "play")
    /// Image `save_active`.
    static let save_active = Rswift.ImageResource(bundle: R.hostingBundle, name: "save_active")
    /// Image `save_noactive`.
    static let save_noactive = Rswift.ImageResource(bundle: R.hostingBundle, name: "save_noactive")
    /// Image `serach`.
    static let serach = Rswift.ImageResource(bundle: R.hostingBundle, name: "serach")
    /// Image `tab_favorite`.
    static let tab_favorite = Rswift.ImageResource(bundle: R.hostingBundle, name: "tab_favorite")
    /// Image `vote-icon`.
    static let voteIcon = Rswift.ImageResource(bundle: R.hostingBundle, name: "vote-icon")

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "Close", bundle: ..., traitCollection: ...)`
    static func close(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.close, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "cap", bundle: ..., traitCollection: ...)`
    static func cap(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.cap, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "no_image", bundle: ..., traitCollection: ...)`
    static func no_image(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.no_image, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "play", bundle: ..., traitCollection: ...)`
    static func play(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.play, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "save_active", bundle: ..., traitCollection: ...)`
    static func save_active(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.save_active, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "save_noactive", bundle: ..., traitCollection: ...)`
    static func save_noactive(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.save_noactive, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "serach", bundle: ..., traitCollection: ...)`
    static func serach(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.serach, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "tab_favorite", bundle: ..., traitCollection: ...)`
    static func tab_favorite(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.tab_favorite, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "vote-icon", bundle: ..., traitCollection: ...)`
    static func voteIcon(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.voteIcon, compatibleWith: traitCollection)
    }
    #endif

    fileprivate init() {}
  }

  /// This `R.reuseIdentifier` struct is generated, and contains static references to 1 reuse identifiers.
  struct reuseIdentifier {
    /// Reuse identifier `MoviesCell`.
    static let moviesCell: Rswift.ReuseIdentifier<MoviesCell> = Rswift.ReuseIdentifier(identifier: "MoviesCell")

    fileprivate init() {}
  }

  fileprivate struct intern: Rswift.Validatable {
    fileprivate static func validate() throws {
      try _R.validate()
    }

    fileprivate init() {}
  }

  fileprivate class Class {}

  fileprivate init() {}
}

struct _R: Rswift.Validatable {
  static func validate() throws {
    #if os(iOS) || os(tvOS)
    try storyboard.validate()
    #endif
  }

  #if os(iOS) || os(tvOS)
  struct storyboard: Rswift.Validatable {
    static func validate() throws {
      #if os(iOS) || os(tvOS)
      try launchScreen.validate()
      #endif
      #if os(iOS) || os(tvOS)
      try movieDetails.validate()
      #endif
      #if os(iOS) || os(tvOS)
      try movieDetails_iPad.validate()
      #endif
      #if os(iOS) || os(tvOS)
      try moviesList.validate()
      #endif
      #if os(iOS) || os(tvOS)
      try moviesList_iPad.validate()
      #endif
      #if os(iOS) || os(tvOS)
      try tabBar.validate()
      #endif
    }

    #if os(iOS) || os(tvOS)
    struct launchScreen: Rswift.StoryboardResourceWithInitialControllerType, Rswift.Validatable {
      typealias InitialController = UIKit.UIViewController

      let bundle = R.hostingBundle
      let name = "LaunchScreen"

      static func validate() throws {
        if #available(iOS 11.0, tvOS 11.0, *) {
        }
      }

      fileprivate init() {}
    }
    #endif

    #if os(iOS) || os(tvOS)
    struct movieDetails: Rswift.StoryboardResourceWithInitialControllerType, Rswift.Validatable {
      typealias InitialController = MovieDetailsViewController

      let bundle = R.hostingBundle
      let movieDetailsViewController = StoryboardViewControllerResource<MovieDetailsViewController>(identifier: "MovieDetailsViewController")
      let name = "MovieDetails"

      func movieDetailsViewController(_: Void = ()) -> MovieDetailsViewController? {
        return UIKit.UIStoryboard(resource: self).instantiateViewController(withResource: movieDetailsViewController)
      }

      static func validate() throws {
        if UIKit.UIImage(named: "cap", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'cap' is used in storyboard 'MovieDetails', but couldn't be loaded.") }
        if UIKit.UIImage(named: "vote-icon", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'vote-icon' is used in storyboard 'MovieDetails', but couldn't be loaded.") }
        if #available(iOS 11.0, tvOS 11.0, *) {
          if UIKit.UIColor(named: "textColor", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Color named 'textColor' is used in storyboard 'MovieDetails', but couldn't be loaded.") }
        }
        if _R.storyboard.movieDetails().movieDetailsViewController() == nil { throw Rswift.ValidationError(description:"[R.swift] ViewController with identifier 'movieDetailsViewController' could not be loaded from storyboard 'MovieDetails' as 'MovieDetailsViewController'.") }
      }

      fileprivate init() {}
    }
    #endif

    #if os(iOS) || os(tvOS)
    struct movieDetails_iPad: Rswift.StoryboardResourceWithInitialControllerType, Rswift.Validatable {
      typealias InitialController = MovieDetailsViewController

      let bundle = R.hostingBundle
      let movieDetailsViewController = StoryboardViewControllerResource<MovieDetailsViewController>(identifier: "MovieDetailsViewController")
      let name = "MovieDetails_iPad"

      func movieDetailsViewController(_: Void = ()) -> MovieDetailsViewController? {
        return UIKit.UIStoryboard(resource: self).instantiateViewController(withResource: movieDetailsViewController)
      }

      static func validate() throws {
        if UIKit.UIImage(named: "cap", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'cap' is used in storyboard 'MovieDetails_iPad', but couldn't be loaded.") }
        if UIKit.UIImage(named: "vote-icon", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'vote-icon' is used in storyboard 'MovieDetails_iPad', but couldn't be loaded.") }
        if #available(iOS 11.0, tvOS 11.0, *) {
        }
        if _R.storyboard.movieDetails_iPad().movieDetailsViewController() == nil { throw Rswift.ValidationError(description:"[R.swift] ViewController with identifier 'movieDetailsViewController' could not be loaded from storyboard 'MovieDetails_iPad' as 'MovieDetailsViewController'.") }
      }

      fileprivate init() {}
    }
    #endif

    #if os(iOS) || os(tvOS)
    struct moviesList: Rswift.StoryboardResourceWithInitialControllerType, Rswift.Validatable {
      typealias InitialController = MoviesListViewController

      let bundle = R.hostingBundle
      let moviesListViewController = StoryboardViewControllerResource<MoviesListViewController>(identifier: "MoviesListViewController")
      let name = "MoviesList"

      func moviesListViewController(_: Void = ()) -> MoviesListViewController? {
        return UIKit.UIStoryboard(resource: self).instantiateViewController(withResource: moviesListViewController)
      }

      static func validate() throws {
        if UIKit.UIImage(named: "cap", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'cap' is used in storyboard 'MoviesList', but couldn't be loaded.") }
        if UIKit.UIImage(named: "save_noactive", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'save_noactive' is used in storyboard 'MoviesList', but couldn't be loaded.") }
        if #available(iOS 11.0, tvOS 11.0, *) {
        }
        if _R.storyboard.moviesList().moviesListViewController() == nil { throw Rswift.ValidationError(description:"[R.swift] ViewController with identifier 'moviesListViewController' could not be loaded from storyboard 'MoviesList' as 'MoviesListViewController'.") }
      }

      fileprivate init() {}
    }
    #endif

    #if os(iOS) || os(tvOS)
    struct moviesList_iPad: Rswift.StoryboardResourceWithInitialControllerType, Rswift.Validatable {
      typealias InitialController = MoviesListViewController

      let bundle = R.hostingBundle
      let moviesListViewController = StoryboardViewControllerResource<MoviesListViewController>(identifier: "MoviesListViewController")
      let name = "MoviesList_iPad"

      func moviesListViewController(_: Void = ()) -> MoviesListViewController? {
        return UIKit.UIStoryboard(resource: self).instantiateViewController(withResource: moviesListViewController)
      }

      static func validate() throws {
        if UIKit.UIImage(named: "cap", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'cap' is used in storyboard 'MoviesList_iPad', but couldn't be loaded.") }
        if UIKit.UIImage(named: "save_noactive", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'save_noactive' is used in storyboard 'MoviesList_iPad', but couldn't be loaded.") }
        if #available(iOS 11.0, tvOS 11.0, *) {
        }
        if _R.storyboard.moviesList_iPad().moviesListViewController() == nil { throw Rswift.ValidationError(description:"[R.swift] ViewController with identifier 'moviesListViewController' could not be loaded from storyboard 'MoviesList_iPad' as 'MoviesListViewController'.") }
      }

      fileprivate init() {}
    }
    #endif

    #if os(iOS) || os(tvOS)
    struct tabBar: Rswift.StoryboardResourceType, Rswift.Validatable {
      let bundle = R.hostingBundle
      let name = "TabBar"
      let tabBarViewController = StoryboardViewControllerResource<TabBarViewController>(identifier: "TabBarViewController")

      func tabBarViewController(_: Void = ()) -> TabBarViewController? {
        return UIKit.UIStoryboard(resource: self).instantiateViewController(withResource: tabBarViewController)
      }

      static func validate() throws {
        if #available(iOS 11.0, tvOS 11.0, *) {
        }
        if _R.storyboard.tabBar().tabBarViewController() == nil { throw Rswift.ValidationError(description:"[R.swift] ViewController with identifier 'tabBarViewController' could not be loaded from storyboard 'TabBar' as 'TabBarViewController'.") }
      }

      fileprivate init() {}
    }
    #endif

    fileprivate init() {}
  }
  #endif

  fileprivate init() {}
}
