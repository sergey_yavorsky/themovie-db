//
//  FileManager.swift
//  themovie-db
//
//  Created by Sergey Yavorsky on 12/11/19.
//  Copyright © 2019 Sergey Yavorsky. All rights reserved.
//

import Foundation

import UIKit
import Foundation

enum FileName: String {
	case favorites = "Favorites.json"
	case movieList = "MovieList.json"
	case movieItem = "MovieItem.json"
}

let TheFileManager = FileWorker.shared

final class FileWorker {
	static let shared = FileWorker()
	static private let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
	static private let documentsDirectoryPath = URL(string: documentsDirectoryPathString)!
	private let fileManager = FileManager.default
	private var isDirectory: ObjCBool = false
	func addItem<T: Encodable>(_ item: T, _ fileName: FileName) {
		if var model = readModel(fileName: fileName, as: MoviesResultsModel.self), model.count > 0 {
			removeFile(fileName)
			model.append(item as! MoviesResultsModel)
			savePool(model, fileName)
		} else {
			savePool([item as! MoviesResultsModel], fileName)
		}
	}
	func savePool<T: Encodable>(_ model: [T], _ fileName: FileName) {
		if isFileExist(fileName) == false {
			if createFile(fileName) {
				writeData(model, fileName)
			}
		}
	}
	func readModel<T: Decodable>(fileName: FileName, as type: T.Type) -> [T]? {
		if isFileExist(fileName) {
			guard let jsonData = retrieveFromJsonFile(fileName) else { return nil }
			do {
				let model = try JSONDecoder().decode([T].self, from: jsonData)
				return model
			} catch {
				print("Attention - JSON is invalid! = ", error.localizedDescription)
			}
		}
		return nil
	}
	func deleteAll(fileName: FileName) {
		removeFile(fileName)
	}
	func delete<T: Codable>(_ model: T, fileName: FileName) {
		let currentItem = model as! MoviesResultsModel
		if var poolModel: [T] = readModel(fileName: fileName, as: T.self) {
			for inx in 0..<poolModel.count {
				let storedModel = poolModel[inx] as! MoviesResultsModel
				if currentItem.id == storedModel.id {
					removeFile(fileName)
					poolModel.remove(at: inx)
					if poolModel.count > 0 {
						savePool(poolModel, fileName)
					}
					break
				}
			}
		}
	}
	func isExist<T: Codable>(_ model: T, fileName: FileName) -> Bool {
		let currentItem = model as! MoviesResultsModel
		if let poolModel: [T] = readModel(fileName: fileName, as: T.self) {
			for inx in 0..<poolModel.count {
				let storedModel = poolModel[inx] as! MoviesResultsModel
				if currentItem.id == storedModel.id {
					return true
				}
			}
		}
		return false
	}
	private func retrieveFromJsonFile(_ fileName: FileName) -> Data? {
		let path = FileWorker.documentsDirectoryPath.appendingPathComponent(fileName.rawValue)
		do {
			let file = try FileHandle(forReadingFrom: path)
			let data: Data = file.readDataToEndOfFile()
			file.closeFile()
			return data
		} catch let error as NSError {
			managerPrint(error)
		}
		return nil
	}
	private func isFileExist(_ fileName: FileName) -> Bool {
		let path = FileWorker.documentsDirectoryPath.appendingPathComponent(fileName.rawValue)
		return fileManager.fileExists(atPath: path.absoluteString, isDirectory: &isDirectory)
	}
	private func createFile(_ fileName: FileName) -> Bool {
		let path = FileWorker.documentsDirectoryPath.appendingPathComponent(fileName.rawValue)
		return fileManager.createFile(atPath: path.absoluteString, contents: nil, attributes: nil)
	}
	private func writeData<T: Encodable>(_ model: [T], _ fileName: FileName) {
		let path = FileWorker.documentsDirectoryPath.appendingPathComponent(fileName.rawValue)
		do {
			let fileHandle = try FileHandle(forWritingTo: path)
			fileHandle.write(encodeModel(model)!)
			fileHandle.closeFile()
		} catch let error as NSError {
			managerPrint(error)
		}
	}
	private func encodeModel<T: Encodable>(_ model: [T]) -> Data? {
		if let jsonData = try? JSONEncoder().encode(model) {
			return jsonData
		}
		return nil
	}
	private func removeFile(_ fileName: FileName) {
		let path = FileWorker.documentsDirectoryPath.appendingPathComponent(fileName.rawValue)
		do {
			try fileManager.removeItem(atPath: path.absoluteString)
		} catch let error as NSError {
			managerPrint(error)
		}
	}
	private func managerPrint(_ error: NSError) {
		print("Couldn't read / write file: \(error.localizedDescription)")
		print("Domain: \(error.domain)")
		print("FailureReason: \(String(describing: error.localizedFailureReason))")
		print("RecoveryOptions: \(String(describing: error.localizedRecoveryOptions))")
	}
}
