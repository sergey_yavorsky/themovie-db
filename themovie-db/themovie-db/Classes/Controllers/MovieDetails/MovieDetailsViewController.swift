//
//  MovieDetailsViewController.swift
//  themovie-db
//
//  Created by Sergey Yavorsky on 12/9/19.
//  Copyright © 2019 Sergey Yavorsky. All rights reserved.
//

import UIKit
import SDWebImage
import MBCircularProgressBar

class MovieDetailsViewController: UIViewController {
	
	// MARK: - Properties & IBOutlets
	
	@IBOutlet weak private var iconImgView: UIImageView!
	@IBOutlet private weak var movieTitleLbl: UILabel!
	@IBOutlet private weak var voteLbl: UILabel!
	@IBOutlet private weak var genresLbl: UILabel!
	@IBOutlet private weak var statusLbl: UILabel!
	@IBOutlet private weak var releaseDateLbl: UILabel!
	@IBOutlet private weak var shortDescriptionLbl: UILabel!
	@IBOutlet private weak var productionCountriesLbl: UILabel!
	@IBOutlet private weak var languagesLbl: UILabel!
	@IBOutlet weak private var progressCircleBarView: MBCircularProgressBarView!
	
	var movieModel: MovieModel?
	
	// MARK: - Lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
		guard let movieModel = self.movieModel else { return }
		
		if let movieTitle = movieModel.title {
			movieTitleLbl.text = movieTitle
		}
		if let voteAverage = movieModel.voteAverage {
			progressCircleBarView.value = CGFloat(voteAverage * 10)
		}
		if let overview = movieModel.overview {
			shortDescriptionLbl.text = "Overview: " + overview
		}
		if let status = movieModel.status, status.count > 0 {
			statusLbl.text = "Status: " + status
		}
		if let voteCount = movieModel.voteCount {
			voteLbl.text = String(voteCount) + " people"
		}
		if let releaseDate = movieModel.releaseDate, releaseDate.count > 0 {
			releaseDateLbl.text = "Date: " + releaseDate
		}
		if let genres = movieModel.genres, genres.count > 0 {
			var genre = String(), idx = 0
			for (index, element) in genres.enumerated() {
				if let name = element.name {
					idx += 1
					genre = genre + name + (index == genres.count - 1 ? "" : ", ")
				}
			}
			if genre.count > 0 {
				genresLbl.text = (idx > 1 ? "Genres: " : "Genre: ") + genre
			}
		}
		if let languages = movieModel.spokenLanguages, languages.count > 0 {
			var language = String(), idx = 0
			for (index, element) in languages.enumerated() {
				if let name = element.name {
					idx += 1
					language = language + name
					if let iso = element.iso31661 {
						language = language + (" (" + iso + ")")
					}
				}
				language = index == languages.count - 1 ? language + "" : language + ", "
			}
			if language.count > 0 {
				languagesLbl.text = (idx > 1 ? "Languages: " : "Language: ") + language
			}
		}
		if let productionCountries = movieModel.productionCountries, productionCountries.count > 0 {
			var countries = String(), idx = 0
			for (index, element) in productionCountries.enumerated() {
				if let name = element.name {
					idx += 1
					countries = countries + name
					if let iso = element.iso31661 {
						countries = countries + (" (" + iso + ")")
					}
				}
				countries = index == productionCountries.count - 1 ? countries + "" : countries + ", "
			}
			if countries.count > 0 {
				productionCountriesLbl.text = "Production " + (idx > 1 ? "countries: " : "country: ") + countries
			}
		}
		if let backdropPath = movieModel.backdropPath {
			let url = URL(string: BaseImageHost + backdropPath)!
			SDWebImageManager.shared.loadImage(with: url,
											   options: .continueInBackground,
											   progress: nil)
			{ [weak self] (image: UIImage?, _, error: Error?, _, _, _)  in
				guard self != nil else { return }
				if let image = image {
					self!.iconImgView.image = image
				}
			}
		}
    }
	
}

