//
//  TabBarViewController.swift
//  themovie-db
//
//  Created by Sergey Yavorsky on 12/10/19.
//  Copyright © 2019 Sergey Yavorsky. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.navigationBar.isHidden = true
		let name = isIPad ? "MoviesList_iPad" : "MoviesList"
		let storyboard = UIStoryboard(name: name, bundle: .main)
		let moviesVC = storyboard.instantiateViewController(withIdentifier: "MoviesListViewController") as! MoviesListViewController
		moviesVC.tabBarItem = UITabBarItem(title: "Movies", image: UIImage(named: "play"), selectedImage: UIImage(named: "play"))
		moviesVC.tabBarItem.tag = 0
		
		let favoritesVC = storyboard.instantiateViewController(withIdentifier: "MoviesListViewController") as! MoviesListViewController
		favoritesVC.isFavorites = true
		favoritesVC.tabBarItem = UITabBarItem(title: "Favorites", image: UIImage(named: "tab_favorite"), selectedImage: UIImage(named: "tab_favorite"))
		favoritesVC.tabBarItem.tag = 1

		let controllerArray = [moviesVC, favoritesVC]
		viewControllers = controllerArray.map{ UINavigationController.init(rootViewController: $0)}
		tabBar.unselectedItemTintColor = UIColor(red: 140/255.0, green: 157/255.0, blue: 200/255.0, alpha: 0.94)
		tabBar.tintColor = UIColor(red: 72/255.0, green: 160/255.0, blue: 232/255.0, alpha: 1.0)
		let font = isIPad ? R.font.sfProTextSemibold(size: 20)! : R.font.sfProTextSemibold(size: 12)!
		UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: font],
																				  for: .normal)
		UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: font],
																				  for: .selected)
	}
	
}
