//
//  MoviesListViewController.swift
//  themovie-db
//
//  Created by Sergey Yavorsky on 12/9/19.
//  Copyright © 2019 Sergey Yavorsky. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage
import MBCircularProgressBar

protocol MoviesListProtocol {
	func refreshMoviesWith(index: Int)
}

class MoviesListViewController: UIViewController, MoviesListProtocol {
	
	// MARK: - Properties & IBOutlets
	
	var isFavorites = false
	
	@IBOutlet private weak var tableView: UITableView!
	
	private var searchBar = UISearchBar()
	private var moviesList: [MoviesResultsModel]?, searchResultsList: [MoviesResultsModel]?
	
	// MARK: - Lifecycle methods
	
	override func viewDidLoad() {
		super.viewDidLoad()
		title = isFavorites ? "Favorites" : "Movies"
		tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
		let font = isIPad ? R.font.sfProTextSemibold(size: 26)! : R.font.sfProTextSemibold(size: 18)!
		navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: font]
		
		searchBar.sizeToFit()
		searchBar.placeholder = "Search"
		searchBar.isHidden = true
		searchBar.delegate = self
	}
	
	override func viewDidAppear(_ animated: Bool) {
		if isFavorites {
			loadMoviesWith(file: .favorites)
		} else {
			if isInternet() {
				loadMoviesList()
			} else {
				loadMoviesWith(file: .movieList)
			}
		}
	}
	
	// MARK: - Private methods
	
	private func showSpinner() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
    }
	private func loadMoviesWith(file: FileName) {
		if let movie = TheFileManager.readModel(fileName: file, as: MoviesResultsModel.self), movie.count > 0 {
			moviesList = movie
			shwoData()
		}
	}
	private func loadIconForCell(_ cell: MoviesCell, _ url: URL) {
        SDWebImageManager.shared.loadImage(with: url,
										   options: .continueInBackground,
										   progress: nil)
		{ [weak self] (image: UIImage?, _, error: Error?, _, _, _)  in
            guard self != nil else { return }
            if let image = image {
                cell.iconImgView.image = image
            }
        }
    }
	@objc private func callMethod() {
		searchBar.text = ""
		searchBar.isHidden = !searchBar.isHidden
		navigationController?.navigationBar.topItem?.titleView = searchBar.isHidden ? nil : searchBar
		if searchBar.isHidden {
			tableView.reloadData()
		}
    }
	private func drawRightBarButton() {
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.setImage(R.image.serach(), for: .normal)
        button.addTarget(self, action:#selector(callMethod), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        let barButton = UIBarButtonItem(customView: button)
        navigationItem.rightBarButtonItem = barButton
    }
	private func shwoData() {
		tableView.reloadData()
		drawRightBarButton()
		tableView.isHidden = false
	}
	private func starConfig(cell: MoviesCell, file: FileName, item: MoviesResultsModel) {
		if TheFileManager.isExist(item, fileName: file) {
			cell.starBtn.tag = 1
			cell.starBtn.setImage(R.image.save_active(), for: .normal)
		} else {
			cell.starBtn.tag = 0
			cell.starBtn.setImage(R.image.save_noactive(), for: .normal)
		}
	}
	private func showDetailsWithMoview(movieModel: MovieModel) {
		let name = isIPad ? "MovieDetails_iPad" : "MovieDetails"
		let storyboard = UIStoryboard(name: name, bundle: .main)
		let vc = storyboard.instantiateViewController(withIdentifier: "MovieDetailsViewController") as! MovieDetailsViewController
		vc.movieModel = movieModel
		navigationController?.pushViewController(vc, animated: true)
	}
	func refreshMoviesWith(index: Int) {
		if isFavorites {
			if searchBar.isHidden {
				moviesList?.remove(at: index)
			} else {
				searchResultsList?.remove(at: index)
			}
		}
		tableView.reloadData()
	}
	
}

// MARK: - UISearchBarDelegate

extension MoviesListViewController: UISearchBarDelegate {
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		sercgWithString(searchText)
	}
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		sercgWithString(searchBar.text!)
	}
	private func sercgWithString(_ string: String) {
		guard let moviesList = moviesList else { return }
		if string.count > 0 {
			searchResultsList = moviesList.filter { item in
				guard let title = item.title else { return false }
				return title.lowercased().contains(string.lowercased())
			}
		}
		tableView.reloadData()
	}
	
}

// MARK: - Connections extension

extension MoviesListViewController {
    
        private func loadMoviesList() {
        showSpinner()
        MoviesProvider.request(.mostPopularMoviesList, completion: { [weak self] result in
            guard let `self` = self else { return }
            switch result {
            case .success(let response):
                if response.statusCode == 200 {
                    do {
                        let moviesModel = try JSONDecoder().decode(MoviesModel.self, from: response.data)
						if let results = moviesModel.results, results.count > 0 {
							self.moviesList = results
							self.shwoData()
							TheFileManager.deleteAll(fileName: .movieList)
							TheFileManager.savePool(results, .movieList)
						}
						MBProgressHUD.hide(for: self.view, animated: true)
                    } catch {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                }
            case .failure(_):
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        })
    }
	private func loadCatertyItemsBy(_ id: Int) {
		showSpinner()
        MoviesProvider.request(.movieInDatailsBy(id: id), completion: { [weak self] result in
            guard let `self` = self else { return }
            switch result {
            case .success(let response):
                if response.statusCode == 200 {
                    do {
                        let movieModel = try JSONDecoder().decode(MovieModel.self, from: response.data)
						TheFileManager.deleteAll(fileName: .movieItem)
						TheFileManager.savePool([movieModel], .movieItem)
						MBProgressHUD.hide(for: self.view, animated: true)
						self.showDetailsWithMoview(movieModel: movieModel)
                    } catch {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                }
            case .failure(_):
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        })
    }
    
}

// MARK: - UITableViewDataSource & UIScrollViewDelegate

extension MoviesListViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if !searchBar.isHidden {
			guard let searchResultsList = searchResultsList else { return 0 }
			return searchResultsList.count
		}
		guard let moviesList = moviesList else { return 0 }
		return moviesList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.item
		let movieItem = searchBar.isHidden ? moviesList![index] : searchResultsList![index]
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.moviesCell, for: indexPath)!
		cell.selectionStyle = .none
		cell.delegate = self
		cell.movieIndex = index
		cell.movieModel = movieItem
		cell.titleLbl.text = movieItem.title
		cell.shortDescriptionTxtView.text = movieItem.overview
		cell.dataLbl.text = movieItem.releaseDate
		if let backdropPath = movieItem.backdropPath {
			let url = URL(string: BaseImageHost + backdropPath)!
			loadIconForCell(cell, url)
		}
		if let voteAverage = movieItem.voteAverage {
			cell.progressCircleBarView.value = CGFloat(voteAverage * 10)
		}
		starConfig(cell: cell, file: .favorites, item: movieItem)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return isIPad ? 300 : 150
    }
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let index = indexPath.item
		if isInternet() {
			if !searchBar.isHidden {
				guard let itemId = searchResultsList![index].id else { return }
				loadCatertyItemsBy(itemId)
			} else {
				guard let itemId = moviesList![index].id else { return }
				loadCatertyItemsBy(itemId)
			}
		} else {
			if let movies = TheFileManager.readModel(fileName: .movieItem, as: MovieModel.self), movies.count > 0 {
				let movieItem = searchBar.isHidden ? moviesList![index] : searchResultsList![index]
				for (_ , element) in movies.enumerated() {
					if element.title == movieItem.title {
						showDetailsWithMoview(movieModel: element)
					}
				}
			}
		}
	}

}

// MARK: - Class cell's

class MoviesCell: UITableViewCell {
	var movieIndex: Int!
	var movieModel: MoviesResultsModel?
	var delegate: MoviesListProtocol?
	
    @IBOutlet weak var titleLbl: UILabel!
	@IBOutlet weak var dataLbl: UILabel!
	@IBOutlet weak var shortDescriptionTxtView: UITextView!
	@IBOutlet weak var iconImgView: UIImageView!
	@IBOutlet weak var starBtn: UIButton!
	@IBOutlet weak var progressCircleBarView: MBCircularProgressBarView!
	
	@IBAction func starAction(_ sender: UIButton) {
		if sender.tag == 1 {
			if let movieModel = movieModel {
				starBtn.tag = 0
				TheFileManager.delete(movieModel, fileName: .favorites)
				starBtn.setImage(R.image.save_noactive(), for: .normal)
			}
		} else {
			if let movieModel = movieModel {
				sender.tag = 1
				TheFileManager.addItem(movieModel, .favorites)
				starBtn.setImage(R.image.save_active(), for: .normal)
			}
		}
		if let delegate = delegate {
			delegate.refreshMoviesWith(index: movieIndex)
		}
	}
    
}
