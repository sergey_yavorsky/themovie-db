//
//  CategoryModel.swift
//  themovie-db
//
//  Created by Work on 07.12.2019.
//  Copyright © 2019 ltst. All rights reserved.
//

import Foundation

struct MovieModel: Codable {
    
	enum CodingKeys: String, CodingKey {
		case adult, backdrop_path, belongs_to_collection, budget, genres, homepage, id, imdb_id, original_language,
		original_title, overview, popularity, poster_path, production_companies, production_countries, release_date,
		revenue, runtime, spoken_languages, status, tagline, title, video, vote_average, vote_count
	}
	
	let adult: Bool?
	let backdropPath: String?
	let belongsToCollection: MovieBelongsToCollectionModel?
	let budget: Int?
	let genres: [MovieGenresModel]?
	let homepage: String?
	let id: Int?
	let imdbId: String?
	let originalLanguage: String?
	let originalTitle: String?
	let overview: String?
	let popularity: Float?
	let posterPath: String?
	let productionCompanies: [MovieProductionCompaniesModel]?
	let productionCountries: [MovieProductionCountriesModel]?
	let releaseDate: String?
	let revenue: Int?
	let runtime: Int?
	let spokenLanguages: [MovieSpokenLanguagesModel]?
	let status: String?
	let tagline: String?
	let title: String?
	let video: Bool?
	let voteAverage: Float?
	let voteCount: Int?
	
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		adult = try container.decode(Bool.self, forKey: .adult)
		backdropPath = try container.decode(String.self, forKey: .backdrop_path)
		let belongs = try container.decode(MovieBelongsToCollectionModel?.self, forKey: .belongs_to_collection)
		if let belongs = belongs {
			belongsToCollection = belongs
		} else {
			belongsToCollection = nil
		}
		
		budget = try container.decode(Int.self, forKey: .budget)
		genres = try container.decode([MovieGenresModel].self, forKey: .genres)
		let home = try container.decode(String?.self, forKey: .homepage)
		if let home = home {
			homepage = home
		} else {
			homepage = ""
		}
		
		id = try container.decode(Int.self, forKey: .id)
		imdbId = try container.decode(String.self, forKey: .imdb_id)
		originalLanguage = try container.decode(String.self, forKey: .original_language)
		originalTitle = try container.decode(String.self, forKey: .original_title)
		overview = try container.decode(String.self, forKey: .overview)
		popularity = try container.decode(Float.self, forKey: .popularity)
		posterPath = try container.decode(String.self, forKey: .poster_path)
		productionCompanies = try container.decode([MovieProductionCompaniesModel].self, forKey: .production_companies)
		productionCountries = try container.decode([MovieProductionCountriesModel].self, forKey: .production_countries)
		releaseDate = try container.decode(String.self, forKey: .release_date)
		revenue = try container.decode(Int.self, forKey: .revenue)
		let run = try container.decode(Int?.self, forKey: .runtime)
		if let run = run {
			runtime = run
		} else {
			runtime = 0
		}
		
		spokenLanguages = try container.decode([MovieSpokenLanguagesModel].self, forKey: .spoken_languages)
		status = try container.decode(String.self, forKey: .status)
		tagline = try container.decode(String.self, forKey: .tagline)
		title = try container.decode(String.self, forKey: .title)
		video = try container.decode(Bool.self, forKey: .video)
		voteAverage = try container.decode(Float.self, forKey: .vote_average)
		voteCount = try container.decode(Int.self, forKey: .vote_count)
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(adult, forKey: .adult)
		try container.encode(backdropPath, forKey: .backdrop_path)
		try container.encode(belongsToCollection, forKey: .belongs_to_collection)
		try container.encode(budget, forKey: .budget)
		try container.encode(genres, forKey: .genres)
		try container.encode(homepage, forKey: .homepage)
		try container.encode(id, forKey: .id)
		try container.encode(imdbId, forKey: .imdb_id)
		try container.encode(originalLanguage, forKey: .original_language)
		try container.encode(originalTitle, forKey: .original_title)
		try container.encode(overview, forKey: .overview)
		try container.encode(popularity, forKey: .popularity)
		try container.encode(posterPath, forKey: .poster_path)
		try container.encode(productionCompanies, forKey: .production_companies)
		try container.encode(productionCountries, forKey: .production_countries)
		try container.encode(releaseDate, forKey: .release_date)
		try container.encode(revenue, forKey: .revenue)
		try container.encode(runtime, forKey: .runtime)
		try container.encode(spokenLanguages, forKey: .spoken_languages)
		try container.encode(status, forKey: .status)
		try container.encode(tagline, forKey: .tagline)
		try container.encode(title, forKey: .title)
		try container.encode(video, forKey: .video)
		try container.encode(voteAverage, forKey: .vote_average)
		try container.encode(voteCount, forKey: .vote_count)
	}
	
}

struct MovieBelongsToCollectionModel: Codable {
	
	enum CodingKeys: String, CodingKey {
		case backdrop_path, id, name, poster_path
	}

	let backdropPath: String?
	let id: Int?
	let name: String?
	let posterPath: String?
	
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		backdropPath = try container.decode(String.self, forKey: .backdrop_path)
		id = try container.decode(Int.self, forKey: .id)
		name = try container.decode(String.self, forKey: .name)
		posterPath = try container.decode(String.self, forKey: .poster_path)
	}
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(backdropPath, forKey: .backdrop_path)
		try container.encode(id, forKey: .id)
		try container.encode(name, forKey: .name)
		try container.encode(posterPath, forKey: .poster_path)
	}
	
}

struct MovieGenresModel: Codable {
	
	let id: Int?
	let name: String?
	
}

struct MovieProductionCompaniesModel: Codable {
	
	enum CodingKeys: String, CodingKey {
		case id, logo_path, name, origin_country
	}
	
	let id: Int?
	let logoPath: String?
	let name: String?
	let originCountry: String?
	
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		id = try container.decode(Int.self, forKey: .id)
		let logo = try container.decode(String?.self, forKey: .logo_path)
		if let logo = logo {
			logoPath = logo
		} else {
			logoPath = ""
		}
		name = try container.decode(String.self, forKey: .name)
		originCountry = try container.decode(String.self, forKey: .origin_country)
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(id, forKey: .id)
		try container.encode(logoPath, forKey: .logo_path)
		try container.encode(name, forKey: .name)
		try container.encode(originCountry, forKey: .origin_country)
	}
	
}

struct MovieProductionCountriesModel: Codable {
	
	enum CodingKeys: String, CodingKey {
		case iso_3166_1, name
	}
	
	let iso31661: String?
	let name: String?
	
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		iso31661 = try container.decode(String.self, forKey: .iso_3166_1)
		name = try container.decode(String.self, forKey: .name)
	}
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(iso31661, forKey: .iso_3166_1)
		try container.encode(name, forKey: .name)
	}
	
}

struct MovieSpokenLanguagesModel: Codable {
	
	enum CodingKeys: String, CodingKey {
		case iso_639_1, name
	}
	
	let iso31661: String?
	let name: String?
	
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		iso31661 = try container.decode(String.self, forKey: .iso_639_1)
		name = try container.decode(String.self, forKey: .name)
	}
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(iso31661, forKey: .iso_639_1)
		try container.encode(name, forKey: .name)
	}
	
}
