//
//  DrinkModel.swift
//  themovie-db
//
//  Created by Work on 07.12.2019.
//  Copyright © 2019 ltst. All rights reserved.
//

import Foundation

struct MoviesModel: Codable {
	
	private enum CodingKeys: String, CodingKey {
		case page, total_results, total_pages, results
	}
    let page: Int?
	let totalResults: Int?
	let totalPages: Int?
	let results: [MoviesResultsModel]?
	
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
        page = try container.decode(Int.self, forKey: .page)
		totalResults = try container.decode(Int.self, forKey: .total_results)
		totalPages = try container.decode(Int.self, forKey: .total_pages)
		results = try container.decode([MoviesResultsModel].self, forKey: .results)
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(page, forKey: .page)
		try container.encode(totalResults, forKey: .total_results)
		try container.encode(totalPages, forKey: .total_pages)
		try container.encode(results, forKey: .results)
	}
}

struct MoviesResultsModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        case popularity, vote_count, video, poster_path, id, adult, backdrop_path, original_language,
		original_title, genre_ids, title, vote_average, overview, release_date
    }
    
    let popularity: Float?
    let voteCount: Int?
    let video: Bool?
	let posterPath: String?
	let id: Int?
	let adult: Bool?
	let backdropPath: String?
    let originalLanguage: String?
	let originalTitle: String?
	let genreIds: [Int]?
	let title: String?
	let voteAverage: Float?
	let overview: String?
	let releaseDate: String?
	
	init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        popularity = try container.decode(Float.self, forKey: .popularity)
        voteCount = try container.decode(Int.self, forKey: .vote_count)
        video = try container.decode(Bool.self, forKey: .video)
		posterPath = try container.decode(String.self, forKey: .poster_path)
		id = try container.decode(Int.self, forKey: .id)
		adult = try container.decode(Bool.self, forKey: .adult)
		backdropPath = try container.decode(String.self, forKey: .backdrop_path)
		originalLanguage = try container.decode(String.self, forKey: .original_language)
		originalTitle = try container.decode(String.self, forKey: .original_title)
		genreIds = try container.decode([Int].self, forKey: .genre_ids)
		title = try container.decode(String.self, forKey: .title)
		voteAverage = try container.decode(Float.self, forKey: .vote_average)
		overview = try container.decode(String.self, forKey: .overview)
		releaseDate = try container.decode(String.self, forKey: .release_date)
    }
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(popularity, forKey: .popularity)
		try container.encode(voteCount, forKey: .vote_count)
		try container.encode(video, forKey: .video)
		try container.encode(posterPath, forKey: .poster_path)
		try container.encode(id, forKey: .id)
		try container.encode(adult, forKey: .adult)
		try container.encode(backdropPath, forKey: .backdrop_path)
		try container.encode(originalLanguage, forKey: .original_language)
		try container.encode(originalTitle, forKey: .original_title)
		try container.encode(genreIds, forKey: .genre_ids)
		try container.encode(title, forKey: .title)
		try container.encode(voteAverage, forKey: .vote_average)
		try container.encode(overview, forKey: .overview)
		try container.encode(releaseDate, forKey: .release_date)
	}
	
}
