//
//  Utils.swift
//  themovie-db
//
//  Created by Sergey Yavorsky on 12/9/19.
//  Copyright © 2019 Sergey Yavorsky. All rights reserved.
//

import UIKit
import Alamofire

var isIPad: Bool {
	return UIDevice.current.userInterfaceIdiom == .pad
}

func isInternet() -> Bool {
	if NetworkReachabilityManager()!.isReachable == true {
		return true
	}
	return false
}
