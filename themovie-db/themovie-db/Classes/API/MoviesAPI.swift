//
//  MoviesAPI.swift
//  themovie-db
//
//  Created by Sergey Yavorsky on 12/6/19.
//  Copyright © 2019 ltst. All rights reserved.
//

import Moya

let MoviesProvider = MoyaProvider<MoviesService>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])

public enum MoviesService {
    case mostPopularMoviesList
    case movieInDatailsBy(id: Int)
}

extension MoviesService: TargetType {
    
    public var baseURL: URL {
        switch self {
        case .mostPopularMoviesList:
            return URL(string: BaseHost + "discover/movie?sort_by=popularity.desc&api_key=" + APIKey)!
        case .movieInDatailsBy(let id):
            return URL(string: BaseHost + "movie/" + String(id) + "?api_key=" + APIKey)!
        }
    }

    public var path: String {
        return ""
    }

    public var method: Moya.Method {
        return .get
    }

    public var task: Task {
        return .requestPlain
    }

    public var validate: Bool {
        return false
    }

    public var sampleData: Data {
        return Data()
    }

    public var headers: [String: String]? {
        switch self {
        default:
            return Headers
        }
    }
    
}
