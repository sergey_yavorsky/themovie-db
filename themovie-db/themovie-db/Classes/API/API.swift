//
//  API.swift
//  themovie-db
//
//  Created by Sergey Yavorsky on 12/6/19.
//  Copyright © 2019 ltst. All rights reserved.
//

import Moya

let BaseHost = "https://api.themoviedb.org/3/"
let BaseImageHost = "https://image.tmdb.org/t/p/w1280/"
let APIKey = "f1e5db303b061cc2abb3f1864400ea85"

public func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data
    }
}

public var Headers: [String : String]? {
    let assigned: [String: String] = [
        "accept": "application/json"
        ]
    return assigned
}
